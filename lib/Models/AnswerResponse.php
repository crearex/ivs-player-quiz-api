<?php
/*
 * AnswerResponse
 */
namespace \Models;

/*
 * AnswerResponse
 */
class AnswerResponse {
    /* @var int $id  */
    private $id;
/* @var string $answer  */
    private $answer;
/* @var int $questionId  */
    private $questionId;
/* @var string $question  */
    private $question;
}
