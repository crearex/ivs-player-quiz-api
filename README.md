# IVSPlayer Quiz API

## Installing

```bash
    git clone https://gitlab.com/creare-x/ivs-player-quiz-api.git \
    cd ivs-player-quiz-api
    composer install && composer update
```

## Starting the API Server 


```bash
    composer start
```

### Probably useful packages: 
* https://packagist.org/packages/entomb/slim-json-api
* https://packagist.org/packages/faapz/pdo
* https://packagist.org/packages/slim/csrf
* tuupola/slim-jwt-auth
* tuupola/slim-basic-auth
* dyorg/slim-token-authentication
* awurth/slim-validation
* dogancelik/slim-json
* adrianfalleiro/slim-cli-runner
* adbario/slim-secure-session-middleware
* zhangshize/slim-facades
* tkhamez/slim-role-auth
* adrianfalleiro/slim-cli-runner
* mhndev/slim-file-response
* darkalchemy/slim-auth !!!
* symfony/var-dumper
* juliangut/slim-doctrine
* 