<?php
declare(strict_types=1);

use App\Application\Actions\Question\FindAllQuestionsAction;
use App\Application\Actions\Question\ViewQuestionAction;

use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Slim\App;

return function (App $app) {
    
    $app->group('/answers', function (Group $group) {
        $group->get('[/]', FindAllAnswersAction::class);
        $group->get('/{id}', ViewAnswerAction::class);
        $group->get('/question/{id}', FindAnswersByQuestionIdAction::class);
        $group->post('[/]', AddAnswerAction::class);
        $group->put('/{id}', UpdateAnswerAction::class);
        $group->delete('/{id}', DeleteAnswerAction::class);
    });

    $app->group('/questions', function (Group $group) {
        $group->get('[/]', FindAllQuestionsAction::class);
        $group->get('/{id}', ViewQuestionAction::class);
        $group->post('[/]', AddQuestionAction::class);
        $group->put('/{id}', UpdateQuestionAction::class);
        $group->delete('/{id}', DeleteQuestionAction::class);

    });

};