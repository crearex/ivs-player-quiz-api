<?php

use Slim\App;

return function(App $app) {
    $container = $app->getContainer();
    $q = $container->get('App\Domain\Question\QuestionRepository');
    $a = $container->get('App\Domain\Answer\AnswerRepository');

    $band = $q->add([1, 'Was ist die beste Rockband']);
    $food = $q->add([2, 'Was ist das beste Essen']);

    $v = $a->add([1, $band, 'Queen']);
    $v = $a->add([2, $band, 'Guns n Roses']);
    $v = $a->add([3, $band, 'ACDC']);

    $v = $a->add([4, $food, 'Germknödel']);
    $v = $a->add([5, $food, 'Schnitzi']);

};