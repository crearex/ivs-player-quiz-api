<?php
declare(strict_types=1);

use App\Domain\Question\QuestionRepository;
use App\Domain\Answer\AnswerRepository;
use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\Answer\InMemoryAnswerRepository;
use App\Infrastructure\Persistence\Question\InMemoryQuestionRepository;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our Repository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        QuestionRepository::class  => \DI\autowire(InMemoryQuestionRepository::class),
        AnswerRepository::class => \DI\autowire(InMemoryAnswerRepository::class),
        UserRepository::class => \DI\autowire(InMemoryUserRepository::class)
    ]);
};