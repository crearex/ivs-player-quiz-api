<?php
declare(strict_types=1);

namespace App\Domain\Answer;

use App\Domain\Question\Question;
use JsonSerializable;

class Answer implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var Question
     */
    private $question;

    /**
     * @var string
     */
    private $answer;

    /**
     * @param int|null  $id
     * @param string    $username
     * @param string    $firstName
     */
    public function __construct(?int $id, Question $question, string $answer)
    {
        $this->id = $id;
        $this->question = $question;
        $this->answer = $answer;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question 
    {
        return $this->question;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer ;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'question' => $this->question,
            'answer' => $this->answer
        ];
    }
}