<?php
declare(strict_types=1);

namespace App\Domain\Answer;

use App\Domain\DomainException\DomainRecordNotFoundException;

class AnswerNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The answer you requested does not exist.';
}