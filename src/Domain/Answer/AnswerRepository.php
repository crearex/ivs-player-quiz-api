<?php
declare(strict_types=1);

namespace App\Domain\Answer;

use App\Domain\Question\Question;

interface AnswerRepository
{
    /**
     * @return Answer[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Answer
     * @throws AnswerNotFoundException
     */
    public function findById(int $id): Answer;

    /**
     * @param Question $question
     * @return array
     * @throws AnswerNotFoundException
     */
    public function findByQuestion(Question $question): array;

    /**
     * @param int32 $id
     * @return array
     * @throws AnswerNotFoundException
     */
    public function findByQuestionId(int $id): array;

    /**
     * @param array $data
     * @return Answer
     */
    public function add(array $data) : Answer;

    /**
     * @param array $data
     * @return Answer
     */
    public function update(array $data) : Answer;

    /**
     * @param int $id
     * @return boolean
     */
    public function delete(int $id): bool;
}