<?php
declare(strict_types=1);

namespace App\Domain\Question;

interface QuestionRepository
{
    /**
     * @return Question[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Question
     * @throws QuestionNotFoundException
     */
    public function findById(int $id): Question;

    /**
     * @param array $data
     * @return Question
     */
    public function add(array $data) : Question;


}