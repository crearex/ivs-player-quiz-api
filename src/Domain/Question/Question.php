<?php
declare(strict_types=1);

namespace App\Domain\Question;
use JsonSerializable;

class Question implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     * @param int|null  $id
     * @param string    $username
     * @param string    $firstName
     */
    public function __construct(int $id, string $question)
    {
        $this->id = $id;
        $this->question = $question;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getQuestion(): string 
    {
        return $this->question;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'question' => $this->question
        ];
    }
}