<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Question;

use App\Domain\Question\Question;
use App\Domain\Question\QuestionNotFoundException;
use App\Domain\Question\QuestionRepository;

class InMemoryQuestionRepository implements QuestionRepository
{
    /**
     * @var Question[]
     */
    private $questions;

    /**
     * InMemoryUserRepository constructor.
     *
     * @param array|null $users
     */
    public function __construct(array $users = null)
    {
        // $this->question = $question ?? [
        //     1 => new Question(1, 'Warum ist die Banane krumm'),
        //     2 => new Question(2, 'Was ist die beste Rockband')
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return array_values($this->questions);
    }

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): Question
    {
        if (!isset($this->questions[$id])) {
            throw new QuestionNotFoundException();
        }

        return $this->questions[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function add(array $data) : Question {
        $question = new Question($data[0], $data[1]);
        $this->questions[$data[0]] = $question;
        return $question;
    }
}