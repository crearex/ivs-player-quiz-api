<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Answer;

use App\Domain\Answer\Answer;
use App\Domain\Answer\AnswerNotFoundException;
use App\Domain\Answer\AnswerRepository;
use App\Domain\Question\Question;

class InMemoryAnswerRepository implements AnswerRepository
{
    /**
     * @var Answer[]
     */
    private $answers;

    /**
     * InMemoryAnswerRepository constructor.
     *
     * @param array|null $Answers
     */
    public function __construct(array $answers = null)
    {
        $this->answers = $answers ?? [];
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return array_values($this->answers);
    }

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): Answer
    {
        if (!isset($this->answers[$id])) {
            throw new AnswerNotFoundException();
        }

        return $this->answers[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findByQuestion(Question $question): array
    {
        $ret = [];
        foreach ($this->answers as $answer) {
            if ($answer->question === $question) {
                $ret[] = $answer;
            }
        }

        return $ret;
    }

     /**
     * {@inheritdoc}
     */
    public function findByQuestionId(int $id) : array {
        $ret = [];
        foreach ($this->answers as $answer) {
            if ($answer->getQuestion()->getId() === $id) {
                $ret[] = $answer;
            }
        }
        
        return $ret;
    }

     /**
     * {@inheritdoc}
     */
    public function add(array $data) : Answer {
        $answer = new Answer($data[0], $data[1], $data[2]);
        $this->answers[$data[0]] = $answer;
        return $answer;
    }

    /**
     * {@inheritdoc}
     */
    public function update(array $data) : Answer {
        $answer = new Answer($data[0], $data[1], $data[2]);
        $this->answers[$data[0]] = $answer;
        return $answer;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id) : bool {
        if (!isset($this->answers[$id])) {
            return false;
        }
        unset($this->answers[$id]);
        return true;
    }




}