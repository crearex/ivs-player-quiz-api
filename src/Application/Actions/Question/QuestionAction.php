<?php

namespace App\Application\Actions\Question;

use App\Application\Actions\Action;
use App\Domain\Question\QuestionRepository;
use Psr\Log\LoggerInterface;

abstract class QuestionAction extends Action
{
    /**
     * @var QuestionRepository
     */
    protected $questionRepository;

    /**
     * @param LoggerInterface $logger
     * @param QuestionRepository $questionRepository
     */
    public function __construct(LoggerInterface $logger,
                                QuestionRepository $questionRepository
    ) {
        parent::__construct($logger);
        $this->questionRepository = $questionRepository;
    }
}