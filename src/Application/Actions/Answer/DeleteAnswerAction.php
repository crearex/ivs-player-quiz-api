<?php
declare(strict_types=1);

namespace App\Application\Actions\Answer;

use App\Domain\Answer\AnswerNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;

class DeleteAnswerAction extends AnswerAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $answerId = (int) $this->resolveArg('id');
        if (!$this->answerRepository->delete($answerId)) {
            throw new AnswerNotFoundException("Couldn't find answer with id {$answerId} to delete. ");
        }

        $this->logger->info("answer with id `${answerId}` was deleted.");

        return $this->respondWithData(null);
    }
}