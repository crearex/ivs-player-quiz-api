<?php
declare(strict_types=1);

namespace App\Application\Actions\Answer;

use App\Application\Actions\Action;
use App\Domain\Answer\AnswerRepository;
use Psr\Log\LoggerInterface;

abstract class AnswerAction extends Action
{
    /**
     * @var AnswerRepository
     */
    protected $answerRepository;

    /**
     * @param LoggerInterface $logger
     * @param AnswerRepository $answerRepository
     */
    public function __construct(LoggerInterface $logger,
                                AnswerRepository $answerRepository
    ) {
        parent::__construct($logger);
        $this->answerRepository = $answerRepository;
    }
}