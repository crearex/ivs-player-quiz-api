<?php
declare(strict_types=1);

namespace App\Application\Actions\Answer;

use Psr\Http\Message\ResponseInterface as Response;

class AddAnswerAction extends AnswerAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = (array) $this->resolveArg('data');
        $answer = $this->answerRepository->add($data);

        $this->logger->info("Answer created.");

        return $this->respondWithData($answer);
    }
}