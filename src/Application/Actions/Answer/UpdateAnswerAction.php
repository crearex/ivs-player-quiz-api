<?php
declare(strict_types=1);

namespace App\Application\Actions\Answer;

use Psr\Http\Message\ResponseInterface as Response;

class UpdateAnswerAction extends AnswerAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = (array) $this->resolveArg('data');
        $answer = $this->answerRepository->update($data);

        $this->logger->info("Answer with id `$answer->getId()` was updated.");

        return $this->respondWithData($answer);
    }
}