<?php
declare(strict_types=1);

namespace App\Application\Actions\Answer;

use Psr\Http\Message\ResponseInterface as Response;

class FindAnswersByQuestionAction extends AnswerAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $questionId = (int) $this->resolveArg('id');
        $answers = $this->answerRepository->findByQuestionId($questionId);

        $this->logger->info("answers list was viewed.");

        return $this->respondWithData($answers);
    }
}