<?php
declare(strict_types=1);

namespace App\Application\Actions\Answer;

use Psr\Http\Message\ResponseInterface as Response;

class ViewAnswerAction extends AnswerAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $answerId = (int) $this->resolveArg('id');
        $answer = $this->answerRepository->findById($answerId);

        $this->logger->info("answer of id `${answerId}` was viewed.");

        return $this->respondWithData($answer);
    }
}