<?php
declare(strict_types=1);

namespace App\Application\Repository;

class NotFoundException extends \Exception
{
    public $message = 'The question you requested does not exist.';
}