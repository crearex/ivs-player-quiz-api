<?php

namespace App\Application\Repository;

class InMemoryPersistence implements Persistence
{
     /**
     * @var object|null
     */
    private $data = [];

    /**
     * @var int|null
     */
    private $lastId = 0;

    public function generateId(): int
    {
        $this->lastId++;

        return $this->lastId;
    }

    public function persist(array $data)
    {
        $this->data[$this->lastId] = $data;
    }

    public function retrieve(int $id): array
    {
        if (!isset($this->data[$id])) {
            throw new NotFoundException(sprintf('No data found for ID %d', $id));
        }

        return $this->data[$id];
    }

    public function delete(int $id)
    {
        if (!isset($this->data[$id])) {
            throw new NotFoundException(sprintf('No data found for ID %d', $id));
        }

        unset($this->data[$id]);
    }
}