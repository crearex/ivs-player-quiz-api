<?php
/**
 * IVSPlayer Quiz REST API
 * @version 1.0.0
 */

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Slim\App();


/**
 * GET findAnswersByQuestionId
 * Summary: Find answers by questionId
 * Notes: Returns Answers
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/answer/findByQuestion/{questionId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findAnswersByQuestionId as a GET method ?');
            return $response;
            });


/**
 * GET findTipsByAnswerId
 * Summary: Find tips by answerId
 * Notes: Returns Tips
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/tip/findByAnswer/{answerId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findTipsByAnswerId as a GET method ?');
            return $response;
            });


/**
 * GET getAllAnswers
 * Summary: Get all answers
 * Notes: 
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/answer', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing getAllAnswers as a GET method ?');
            return $response;
            });


/**
 * GET getAnswerById
 * Summary: Find answer by id
 * Notes: Returns a single Answer
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/answer/{answerId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing getAnswerById as a GET method ?');
            return $response;
            });


/**
 * GET findAllQuestions
 * Summary: Find All Question
 * Notes: 
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/question', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findAllQuestions as a GET method ?');
            return $response;
            });


/**
 * GET findAnswersByQuestionId
 * Summary: Find answers by questionId
 * Notes: Returns Answers
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/answer/findByQuestion/{questionId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findAnswersByQuestionId as a GET method ?');
            return $response;
            });


/**
 * GET findTipsByQuestionId
 * Summary: Find tips by questionId
 * Notes: Returns tips
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/tip/findByQuestion/{questionId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findTipsByQuestionId as a GET method ?');
            return $response;
            });


/**
 * GET getQuestionById
 * Summary: Find question by id
 * Notes: Returns a single question
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/question/{questionId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing getQuestionById as a GET method ?');
            return $response;
            });


/**
 * POST addTip
 * Summary: Add a Tip
 * Notes: 

 */
$app->POST('/quiz/api/tip', function($request, $response, $args) {
            
            
            
            $body = $request->getParsedBody();
            $response->write('How about implementing addTip as a POST method ?');
            return $response;
            });


/**
 * GET findTipsByAnswerId
 * Summary: Find tips by answerId
 * Notes: Returns Tips
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/tip/findByAnswer/{answerId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findTipsByAnswerId as a GET method ?');
            return $response;
            });


/**
 * GET findTipsByQuestionId
 * Summary: Find tips by questionId
 * Notes: Returns tips
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/tip/findByQuestion/{questionId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing findTipsByQuestionId as a GET method ?');
            return $response;
            });


/**
 * GET getAllTips
 * Summary: Get all tips
 * Notes: 
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/tip', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing getAllTips as a GET method ?');
            return $response;
            });


/**
 * GET getTipById
 * Summary: Find tip by id
 * Notes: Returns a single tip
 * Output-Formats: [application/json]
 */
$app->GET('/quiz/api/tip/{tipId}', function($request, $response, $args) {
            
            
            
            
            $response->write('How about implementing getTipById as a GET method ?');
            return $response;
            });


/**
 * PUT updateTip
 * Summary: Update a Tip
 * Notes: 

 */
$app->PUT('/quiz/api/tip', function($request, $response, $args) {
            
            
            
            $body = $request->getParsedBody();
            $response->write('How about implementing updateTip as a PUT method ?');
            return $response;
            });



$app->run();
